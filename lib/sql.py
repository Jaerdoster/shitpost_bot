# main SQL calls
import sqlite3


def execute_sql(db_path, sql_command, *sql_args):
    con = None
    data = None
    try:
        con = sqlite3.connect(db_path)
        c = con.cursor()
        c.execute(sql_command, tuple(sql_args))
        data = c.fetchall()
        if not data:
            con.commit()
    except sqlite3.Error as e:
        print("Database error: %s" % e)
    except Exception as e:
        print("Exception in _query: %s" % e)
    finally:
        if con:
            con.close()
    return data


def init_database(db_path):
    # create table list
    SQL_create_table_list = '''
                            CREATE TABLE list
                            ( id INTEGER PRIMARY KEY AUTOINCREMENT
                            , name text NOT NULL UNIQUE
                            , comment text 
                            , type text
                            , date_added DEFAULT (DATETIME('now'))
                            , last_post_date datetime
                            , do_not_post integer DEFAULT 0 );
                            '''
    execute_sql(db_path, SQL_create_table_list)
    print("table 'list' created")
    # create table list
    SQL_create_table_posted = '''                
                            CREATE TABLE posted
                            ( id INTEGER PRIMARY KEY AUTOINCREMENT
                            , list_id INTEGER NOT NULL
                            , status_id text
                            , post_date datetime NOT NULL );   '''
    execute_sql(db_path, SQL_create_table_posted)
    print("table 'posted' created")

#get full list from the list table
def get_full_list(db_path):
    SQL_full_list = "SELECT name FROM list"
    return execute_sql(db_path, SQL_full_list)


#disable all medias files
def disable_files(db_path):
    SQL_disable_command = "UPDATE list SET do_not_post = 1 WHERE type = 'file'"
    execute_sql(db_path, SQL_disable_command)

#activate a specific line
def update_list_dnp(db_path, sql_args):
    SQL_list_dnp = "UPDATE list SET do_not_post = 0 WHERE name = ?"
    execute_sql(db_path, SQL_list_dnp, sql_args)

#insert a new line
def insert_into_list(db_path, *sql_args):
    SQL_insert_list = "INSERT INTO list(name,type,do_not_post) VALUES (?,?,0)"
    #print (sql_args)
    execute_sql(db_path, SQL_insert_list, *sql_args)

#get ready to post list
def get_list_to_post(db_path):
    SQL_list_to_post = '''  select id, name, type
                            from list 
                            where do_not_post = 0                    
                            order by (CASE last_post_date WHEN NULL THEN 1 ELSE 0 END)
                            , last_post_date asc
                            , date_added asc
                            LIMIT 10'''
    return execute_sql(db_path, SQL_list_to_post)

#insert last post date and update
def insert_posted(db_path, *sql_args):    
    SQL_update_list = "UPDATE list SET last_post_date = (DATETIME('now')) WHERE id = ?"   
    execute_sql(db_path, SQL_update_list, sql_args[0])
    SQL_insert_posted = "INSERT INTO posted (list_id, post_date, status_id) VALUES(?,?,?)"
    execute_sql(db_path, SQL_insert_posted, *sql_args)



