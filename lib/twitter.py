##main code to authenticate and post on twitter
import tweepy
import json
import os

def twitter_connect(tokens):
    # read the tokens file    
    with open(tokens) as f:
        tokens = json.load(f)
    try:
        # Authenticate to Twitter
        auth = tweepy.OAuthHandler(
            tokens["CONSUMER_KEY"], tokens["CONSUMER_SECRET"])
        auth.set_access_token(
            tokens["ACCESS_TOKEN"], tokens["ACCESS_TOKEN_SECRET"])
        # Create API object
        api = tweepy.API(auth)
    except tweepy.TweepError as e:
        error = "An error number {} occured with twitter, please check \n 'https://dev.twitter.com/overview/api/response-codes' to understand why".format(e.api_code)
        print(error)
    else:
        return api

def post_text(tokens, status_text):
    try:
        a = twitter_connect(tokens)
        s = a.update_status(status_text)
    except tweepy.TweepError as e:
        error = "An error number {} occured with twitter, please check \n 'https://dev.twitter.com/overview/api/response-codes' to understand why".format(e.api_code)
        raise tweepy.TweepError(error)
    else:
        return s

def retweet_twitter(tokens, tid):
    try:
        a = twitter_connect(tokens)
        a.unretweet(tid)   #we unretweet before Retweet again
        s = a.retweet(tid)
    except tweepy.TweepError as e:
        error = "An error number {} occured with twitter, please check \n 'https://dev.twitter.com/overview/api/response-codes' to understand why".format(e.api_code)
        raise tweepy.TweepError(error)
    else:
        return s


def post_media(tokens, media):    
    try:
        a = twitter_connect(tokens)
        s = a.update_with_media(media)
    except tweepy.TweepError as e:
        error = "An error number {} occured with twitter, please check \n 'https://dev.twitter.com/overview/api/response-codes' to understand why".format(e.api_code)
        raise tweepy.TweepError(error)
    else:
        return s
