##First launch only, won't be launched if a config.json file exist
import json
import os

#config.json creation
def create_config(config_path):    
    #we still check if the file exist, in case something fucky happened between the call and now, or just someone who call it outside of main_code.py
    if os.path.exists(config_path):
        print("config.json already exist.")
    else:        
        config = {
            "DATABASE" : "data/shitpost_db.sqlite3",
            "LINK_FILE" : "data/links.txt",
            "IMAGE_FOLDER" : "data/images",
            "BLACKLIST" : "data/blacklist"
            }
        with open(config_path, 'x') as config_file:
            json.dump(config, config_file)
        print("config.json has been created. \n Please fill it with the desired values") 

#img folder creation
def create_img_folder(img_folder):    
    if not os.path.exists(img_folder):            
        os.makedirs(img_folder)
        print("The image folder has been created")
    else:
        print(f"The folder {img_folder} already exist")
