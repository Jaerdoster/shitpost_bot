#main functions, out of the main script for clarity
from lib import init
from lib import sql
from lib import twitter
import os
import json
import random
import datetime

#get the config. return None if it create a new config file to abort the main script
def get_config(config_path):
    if not os.path.exists(config_path):
        # we stop the script after that, because user could want to configure his files and folders.
        init.create_config(config_path)
        return None
    else:
        conf = {}
        with open(config_path) as f:
            try:
                config = json.load(f)
                conf['database'] = os.path.abspath(config["DATABASE"])
                conf['link_file'] = os.path.abspath(config["LINK_FILE"])
                conf['image_folder'] = os.path.abspath(config["IMAGE_FOLDER"])
                conf['blacklist'] = os.path.abspath(config["BLACKLIST"])
            except KeyError:
                print("Config file is incomplete or corrupted.\nPlease update it or delete it to resetup.")
            else:
                return conf

#check if folder or files exists, create it or raise alert if necessary
def check_if_exist(config):
    # check folders
    if not os.path.exists(config["image_folder"]):
        init.create_img_folder(config["image_folder"])
    if not os.path.exists(config["link_file"]):
        print("Warning: link file does not exist. \nPlease create it and set it in the config.json file")
    # initialize database
    if not os.path.exists(config["database"]):
        sql.init_database(config["database"])

#get all files from media folder, and apply the blacklist to it
def get_all_files(config):
    # read image folder
    topost = []    
    for f in os.walk(config["image_folder"]):
        for file in f:
            topost.append(str(file) + "|file")

    # Apply blacklist to files
    bl = []
    if os.path.exists(config["blacklist"]):
        b = open(config["blacklist"])
        b1 = b.readlines()
        for f in b1:
            bl.append(str(f).replace('\n', '').replace('\r', ''))
        topost_bl = []
        b.close()
        for b in bl:
            topost_bl = topost_bl + \
                list([e for e in topost if e.startswith(b)])
        if len(topost_bl) > 0:
            for i in topost_bl:
                topost.remove(i)        
    return topost

# fetch all links and compare it with the database
def get_all_links(config, lst):    
    links = []
    if os.path.exists(config["link_file"]):
        l = open(config["link_file"])
        l1 = l.readlines()
        for link in l1:
            l2 = str(link.replace('\n', '').replace('\r', ''))
            # we check if it's a twitter link
            if l2 not in lst:
                if "twitter" in l2 and "/status/" in l2:
                    links.append(l2 + "|tweet")
                else:
                    links.append(l2 + "|link")
        l.close()
    return links

#insert or update in the database
def insert_and_update(config, topost, lst):
    added = 0
    updated = 0
    if len(topost) > 0:
        for i in topost:
            a, b = i.split("|")
            if a in lst: #we compare with the list extracted from the database, and we update if necessary
                sql.update_list_dnp(config["database"], (a))
                updated += 1
            else:
                args = (a, b)
                sql.insert_into_list(config["database"], *args)
                added += 1
        print(str(added) + " elements added, " +    str(updated) + " elements updated")
    else:
        print("No new elements to add or update")

#fetch what to post from the DB
def get_what_to_post(config):
    rows = sql.get_list_to_post(config["database"])
    if len(rows) == 0:
        the_chosen_one = None
        print("Nothing to post yet. Please fill links file or image folder, or check database.")
    else:
        the_chosen_one = random.choice(rows)
        print(the_chosen_one)
    return the_chosen_one

#post shit to twitter
def post_to_twitter(config, the_chosen_one, tokens):
    # Now we check if it's an image or a link
    if the_chosen_one[2] == "link":                    
        status = twitter.post_text(tokens, the_chosen_one[1])
    elif the_chosen_one[2] == "tweet":
        tid = the_chosen_one[1].split("/status/")[-1]  # we only need the tid                    
        status = twitter.retweet_twitter(tokens, tid)
    else:
        status = twitter.post_media(tokens, os.path.join(config["image_folder"], the_chosen_one[1]))
    # And we update the database to add the last post date
    args = (the_chosen_one[0], datetime.datetime.now(), str(status.id))
    sql.insert_posted(config["database"], *args)