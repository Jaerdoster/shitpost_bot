Jaerdoster's Shitpost Bot

This document is in French. For english version, I will write it later.

1. Présentation rapide:

Ce script alimente un bot twitter en images et liens à tweeter ou retweeter de façon régulière.

Il est écrit entièrement en python.

Son développement a été gelé pour la 1.0, je le reprendrais seulement si besoin, ou si la librairie tweepy est cassée.

2. Dépendances:

Ce script dépend de la librairie Tweepy (https://www.tweepy.org/).

 - Elle est à installer via pip avec la commande suivante: `pip install tweepy`.
 - Vous pouvez également installer en faisant `pip install -r requirements.txt` à la base du projet.

Utiliser un venv est conseillé mais pas obligatoire.

A noter également que bien que je ne pense pas avoir utilisé de librairies spécifiques python 3, je conseille de l'utiliser plutot que son prédecesseur.

Il utilise également sqlite

3. Fonctionnement:

Le script utilise les élements suivants:
 - Un fichier `tokens.json` qui contient les tokens d'authentification à l'API twitter (se référer à la documentation twitter Dev pour obtenir ces tokens). Ce fichier est à créer manuellement, sa structure se trouve dans le fichier .example.
 - Un dossier `config.json` qui contient l'emplacement de la base de donnée, des images et du fichier des liens. Les dossiers ne sont créés automatiquement et la bdd sont créés automatiquement après la création du fichier config, et une alerte est levée si le fichier `links.txt` n'existe pas.
 - Un fichier `links.txt` qui contient tous les liens à poster.
 - Toutes les images ou vidéos stockées dans le dossier `images`.
 - Une base de données sqlite3 qui a par défaut le nom `shitpost_db.sqlite3`.
 - Un fichier blacklist (optionnel) qui contient des noms de fichiers (global ou début de nom seulement) à ne pas charger en base (pour eviter les fichiers thumb.db et cie.)

4. Structure globale:

 - Au démarrage, le script vérifie que la structure des dossiers est correcte, et les créé si nécessaire.
 - Ensuite il crée la base de données et les tables de celle ci, ci elle n'existe pas déjà.
   - Note: à l'heure actuelle, aucune vérification de la structure de la base est faite, on est en WIP.
 - Il récupère les données (ici des liens ou des noms de fichiers) déjà présentes dans la table `list`, et les stocke.
 - Il liste ensuite les données présentes dans le fichier `links.txt` et le dossier `images`, et les compare avec la base:
 - Si les données y sont déjà, elles sont ignorées.
 - Sinon, elles sont ajoutées à la base après avoir été classifiées entre `file`,`link` et `tweet`.
 - Derrière, on extrait 10 données n'ayant jamais été postées, ou alors ayant été postées depuis le plus longtemps, et on en choisit une au hasard.
 - Selon le type de donnée, on tweet l'image ou le lien, ou l'on RT le tweet.
   - Note: on tente au préalable de unretweet le tweet avant de le retweeter à nouveau.
 - Enfin on met à jour la base en ajoutant la date de dernier post a la donnée qui vient d'être tweetée, et on enregistre l'id du tweet au cas ou dans la table `posted`.

5. Crédits

Moi même, @Jaerdoster, et mon Google-fu.
