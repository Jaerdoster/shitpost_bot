import random
from lib import init
from lib import sql
from lib import twitter
from lib import main_functions as mf

# get config
config = mf.get_config("config.json")
if config is not None:
    #we check if necessary folders and database are created already, and create it otherwise    
    mf.check_if_exist(config)
    # Fetch all data from the list table to update it
    lst = []
    rows = sql.get_full_list(config["database"])
    for row in rows:
        lst.append(row[0])

    #fetch all medias
    medias = mf.get_all_files(config)

    # fetch all links and compare it with the database
    links = mf.get_all_links(config, lst)

    # Media files delete prevention
    # We disable all medias, then we will reenable the ones still present in the file system later
    sql.disable_files(config["database"])

    # we destroy duplicates in the list by using set()
    t = medias + links
    topost = list(set(t))
    # we shuffle the list before inserting
    random.shuffle(topost)

    # We add new elements to the db or update the medias
    mf.insert_and_update(config, topost, lst)

    # Fetch some data from the list table to choose the one to post
    the_chosen_one = mf.get_what_to_post(config)

    if the_chosen_one is not None:  # No reason to post nothing
        mf.post_to_twitter(config, the_chosen_one, "tokens.json")